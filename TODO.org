# Track List #

* Progress [18%]
  - [X] Add the ~DeclareOptionPackage~ working with ~book~
  - [X] ~PassOptionToPackage~ for the ~babel~ package: Allow ~spanish~ and ~english~ options.
  - [ ] Use ~xparse~ to redefine the command ~\CC~ to change the license according to the arguments
  - [-] Add pdfbookmarks automatically [3/4]
    - [X] Titlepage
    - [X] Committee
    - [X] Table of Contents
    - [ ] Bibliography
  - [ ] Add the third CC license, including logo
  - [ ] Make the license comment to change when ~spanish~ option is used with ~babel~.
  - [ ] Create new global option ~nocolor~, for black-and-white printing
  - [ ] Make the parenthesis on affiliations of committee members appear if the option is not empty.
  - [ ] How to change the format of date
  - [ ] Change the generation of the titlepage... Use the ~\maketitle~ command?
  - [ ] Check global options are functional

* DOCUMENTATION [0%]	
  - [ ] New packages ~lmodern~ and ~etoolbox~
  - [ ] About the compilation
  - [ ] Explain global options
     
